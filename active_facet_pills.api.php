<?php

/**
 * @file
 * Hooks provided by the Active Facet Pills module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the Active Facet Pills.
 *
 * Modules may implement this hook to alter the links rendered in the Active
 * Facet Pills block.
 *
 * @param array|null $facet_links
 *   The facet links to be displayed on the block.
 */
function hook_active_facet_pills_links_alter(&$facet_links) {
  unset($facet_links['my_unwanted_facet_link']);
}
