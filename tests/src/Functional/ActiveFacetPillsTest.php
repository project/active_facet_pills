<?php

namespace Drupal\Tests\active_facet_pills\Functional;

/**
 * Tests the active pills block display.
 *
 * @group active_facet_pills
 */
class ActiveFacetPillsTest extends ActiveFacetPillsBase {

  /**
   * Test the active facet pills functionality.
   */
  public function testActiveFacetPills() {
    // Enable translation of entity ids to its names for readability.
    $this->drupalGet($this->facetEditPage);
    $edit = [
      'facet_settings[translate_entity][status]' => TRUE,
    ];
    $this->drupalPostForm(NULL, $edit, 'Save');
    $this->drupalGet('search-api-test-fulltext');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', 'a[data-drupal-facet-item-id="taxonomy_facet-1"]');
    $this->assertSession()->elementNotExists('css', 'a.facet-pills__pill__link');
    $this->clickLink($this->terms[0]->label());
    $this->assertSession()->elementContains('css', 'a.facet-pills__pill__link', $this->terms[0]->label());
  }

}
