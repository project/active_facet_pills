<?php

namespace Drupal\Tests\active_facet_pills\Functional;

use Drupal\Core\Language\LanguageInterface;
use Drupal\search_api\Item\Field;
use Drupal\taxonomy\Entity\Term;
use Drupal\Tests\facets\Functional\FacetsTestBase;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;
use Drupal\Tests\taxonomy\Functional\TaxonomyTestTrait;

/**
 * Base class for the active pills tests.
 *
 * @group active_facet_pills
 */
class ActiveFacetPillsBase extends FacetsTestBase {

  use TaxonomyTestTrait;
  use EntityReferenceTestTrait;

  /**
   * The field name for the referenced term.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * Drupal vocabulary.
   *
   * @var \Drupal\taxonomy\Entity\Vocabulary
   */
  protected $vocabulary;

  /**
   * Uri to the facets edit page.
   *
   * @var string
   */
  protected $facetEditPage;

  /**
   * An array of taxonomy terms.
   *
   * @var \Drupal\taxonomy\Entity\Term[]
   */
  protected $terms = [];

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'active_facet_pills',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    // Make sure we're logged in with a user that has sufficient permissions.
    $this->drupalLogin($this->adminUser);

    $this->vocabulary = $this->createVocabulary();
    $this->insertExampleTerms();

    // Default content that is extended with a term reference field below.
    $this->setUpExampleStructure();

    // Create a taxonomy_term_reference field on the article and item.
    $this->fieldName = 'tax_ref_field';
    $fieldLabel = 'Taxonomy reference field';

    $this->createEntityReferenceField('entity_test_mulrev_changed', 'article', $this->fieldName, $fieldLabel, 'taxonomy_term');
    $this->createEntityReferenceField('entity_test_mulrev_changed', 'item', $this->fieldName, $fieldLabel, 'taxonomy_term');

    $this->insertExampleContent();

    // Add fields to index.
    $index = $this->getIndex();

    // Index the taxonomy and entity reference fields.
    $term_field = new Field($index, $this->fieldName);
    $term_field->setType('integer');
    $term_field->setPropertyPath($this->fieldName);
    $term_field->setDatasourceId('entity:entity_test_mulrev_changed');
    $term_field->setLabel($fieldLabel);
    $index->addField($term_field);

    $index->save();
    $this->indexItems($this->indexId);

    $facet_name = 'taxonomy facet';
    $facet_id = 'taxonomy_facet';
    $this->facetEditPage = 'admin/config/search/facets/' . $facet_id . '/edit';

    $this->createFacet($facet_name, $facet_id, $this->fieldName);

    // Place the active pills block.
    $block = [
      'region' => 'footer',
    ];
    $this->blocks['active_facet_pills'] = $this->drupalPlaceBlock('active_facet_pills', $block);

    // Make absolutely sure the ::$blocks variable doesn't pass information
    // along between tests.
    $this->blocks = NULL;
  }

  /**
   * Creates the needed terms for the test.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function insertExampleTerms() {
    for ($i = 0; $i < 2; $i++) {
      $this->terms[] = $this->createTerm();
    }
  }

  /**
   * Creates and returns a taxonomy term.
   *
   * @param array $settings
   *   (optional) An array of values to override the following default
   *   properties of the term:
   *   - name: A random string.
   *   - description: A random string.
   *   - format: First available text format.
   *   - vid: Vocabulary ID of self::$vocabulary object.
   *   - langcode: LANGCODE_NOT_SPECIFIED.
   *   Defaults to an empty array.
   *
   * @return \Drupal\taxonomy\Entity\Term
   *   The created taxonomy term.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createTerm(array $settings = []) {
    $filter_formats = filter_formats();
    $format = array_pop($filter_formats);
    $settings += [
      'name' => $this->randomMachineName(),
      'description' => $this->randomMachineName(),
      // Use the first available text format.
      'format' => $format->id(),
      'vid' => $this->vocabulary->id(),
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ];
    $term = Term::create($settings);
    $term->save();
    return $term;
  }

  /**
   * Creates several test entities with the term-reference field.
   */
  protected function insertExampleContent() {
    $entity_test_storage = \Drupal::entityTypeManager()
      ->getStorage('entity_test_mulrev_changed');

    $this->entities[1] = $entity_test_storage->create([
      'name' => 'foo bar baz',
      'body' => 'test test',
      'type' => 'item',
      'keywords' => ['orange'],
      'category' => 'item_category',
      $this->fieldName => [$this->terms[0]->id()],
    ]);
    $this->entities[1]->save();

    $this->entities[2] = $entity_test_storage->create([
      'name' => 'foo test',
      'body' => 'bar test',
      'type' => 'item',
      'keywords' => ['orange', 'apple', 'grape'],
      'category' => 'item_category',
      $this->fieldName => [$this->terms[0]->id()],
    ]);
    $this->entities[2]->save();

    $this->entities[3] = $entity_test_storage->create([
      'name' => 'bar',
      'body' => 'test foobar',
      'type' => 'item',
      $this->fieldName => [$this->terms[0]->id()],
    ]);
    $this->entities[3]->save();

    $this->entities[4] = $entity_test_storage->create([
      'name' => 'foo baz',
      'body' => 'test test test',
      'type' => 'article',
      'keywords' => ['apple', 'strawberry', 'grape'],
      'category' => 'article_category',
      $this->fieldName => [$this->terms[0]->id()],
    ]);
    $this->entities[4]->save();

    $this->entities[5] = $entity_test_storage->create([
      'name' => 'bar baz',
      'body' => 'foo',
      'type' => 'article',
      'keywords' => ['orange', 'strawberry', 'grape', 'banana'],
      'category' => 'article_category',
      $this->fieldName => [$this->terms[0]->id()],
    ]);
    $this->entities[5]->save();

    $this->entities[6] = $entity_test_storage->create([
      'name' => 'bar baz',
      'body' => 'foo',
      'type' => 'article',
      'keywords' => ['orange', 'strawberry', 'grape', 'banana'],
      'category' => 'article_category',
      $this->fieldName => [$this->terms[1]->id()],
    ]);
    $this->entities[6]->save();
  }

}
